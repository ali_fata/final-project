﻿using System;
using System.IO;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using Microsoft.Win32;
using System.Text;

namespace project.Properties
{
    public partial class Create_Customer : Window
    {
        public Create_Customer()
        {
            InitializeComponent();
        }
        private void btnLoad_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog op = new OpenFileDialog();
            op.Title = "Select a picture";
            op.Filter = "All supported graphics|*.jpg;*.jpeg;*.png|" +
              "JPEG (*.jpg;*.jpeg)|*.jpg;*.jpeg|" +
              "Portable Network Graphic (*.png)|*.png";
            if (op.ShowDialog() == true)
                imgPhoto.Source = new BitmapImage(new Uri(op.FileName));
        }
        public List<Customer> customers = new List<Customer>();
        private void BackToMain_Click(object sender, RoutedEventArgs e)
        {
            Hide();
            new MainWindow().Show();
            Close();
        }
        private void OK_Click(object sender, RoutedEventArgs e)
        {
            string firstname = null, lastname = null, phone_number = null, email = null, national_ID = null, pass = null, address = null, pass2 = null;
            firstname = FirstName.Text; lastname = LastName.Text; phone_number =number.Text; email = Email.Text; national_ID = ID.Text; pass = Pass.Text;  address = Adress.Text; pass2 = Pass2.Text;
            try
            {
                if (!Customer.check_email(email))
                    MessageBox.Show("Invalid Email !", "Alert", MessageBoxButton.OK, MessageBoxImage.Error);
                else if (!Customer.check_Password(pass))
                    MessageBox.Show("Password must contains every characters including numbers,letters,marks,...", "Alert", MessageBoxButton.OK, MessageBoxImage.Error);
                else if (pass != pass2)
                    MessageBox.Show("passwords don't match", "Alert", MessageBoxButton.OK, MessageBoxImage.Error);
                else if (!national_ID.Check_national_ID())
                    MessageBox.Show("Invalid National ID !", "Alert", MessageBoxButton.OK, MessageBoxImage.Error);
                else if (!Customer.check_PhoneNumber(phone_number))
                    MessageBox.Show("Invalid phone number !", "Alert", MessageBoxButton.OK, MessageBoxImage.Error);
                else if (firstname == null || lastname == null || phone_number == null || email == null || national_ID == null || pass == null || address == null)
                    MessageBox.Show("Fill all of the nescessary items", "Alert", MessageBoxButton.OK, MessageBoxImage.Error);
                else
                {
                    try
                    {
                        Customer customer = new Customer(firstname, lastname, phone_number, email, national_ID, pass, address, imgPhoto);
                        customers.Add(customer);
                        Customer.SaveToCustomersFile(customer);
                    }
                    catch
                    {
                        Customer customer = new Customer(firstname, lastname, phone_number, email, national_ID, pass, address);
                        customers.Add(customer);
                        Customer.SaveToCustomersFile(customer);
                    }
                    MessageBox.Show("You successfully created your account \nNow you can log in as a customer");
                }
            }
            catch { MessageBox.Show("enter the information in the correct form"); }
            if (imgPhoto.Source != null)
            {
                SaveFileDialog f = new SaveFileDialog();
                f.Filter = "All supported graphics|*.jpg;*.jpeg;*.png|" + "JPEG (*.jpg;*.jpeg)|*.jpg;*.jpeg|" + "Portable Network Graphic (*.png)|*.png";
                var encoder = new PngBitmapEncoder();
                encoder.Frames.Add(BitmapFrame.Create((BitmapSource)imgPhoto.Source));
                using (FileStream stream = new FileStream(@"C:\Users\TTM\Desktop\final project\project\project\bin\Debug\Customers\"+FirstName.Text+" "+LastName.Text+".jpg", FileMode.Create))
                { encoder.Save(stream); stream.Close(); }
            }
        }
        private void Login_Click(object sender, RoutedEventArgs e)
        {
            Hide();
            Login_Customer window = new Login_Customer();
            window.Show();
            Close();
        }
    }
    static class Extention
    {
        public static bool Check_national_ID(this string str)
        {
            int[] ID = new int[str.Length];
            for (int i = 0; i < str.Length; i++)
                ID[i] = Convert.ToInt32(str[i]) - 48;
            int a = ID[9];
            int b = (ID[0] * 10) + (ID[1] * 9) + (ID[2] * 8) + (ID[3] * 7) + (ID[4] * 6) + (ID[5] * 5) + (ID[6] * 4) + (ID[7] * 3) + (ID[8] * 2);
            int c = b % 11;
            bool tekrari = true;
            for (int i = 0; i < ID.Length; i++)
                if (ID[0] != ID[i])           //if all the digits are the same, like 999999999
                    tekrari = false;
            if (tekrari)
                return false;
            if (c == 0 && a == c)
                return true;
            else if (c == 1 && a == 1)
                return true;
            else if (c > 1 && a == Math.Abs(c - 11))
                return true;
            else
                return false;
        }
    }
    public class Customer
    {
        public string FirstName, phoneNumber, LastName, email, National_ID, password, address;
        public Image ProfilePicture;
        public Customer(string FirstName, string LastName, string PN, string email, string National_ID, string password, string address)
        {
            this.FirstName = FirstName;             // first cunstructor
            this.LastName = LastName;
            phoneNumber = PN;
            this.email = email;
            this.National_ID = National_ID;
            this.password = password;
            this.address = address;
        }
        public Customer(string FirstName, string LastName, string PN, string email, string National_ID, string password, string address, Image image)
        {
            this.FirstName = FirstName;             // second cunstructor
            this.LastName = LastName;
            phoneNumber = PN;
            this.email = email;
            this.National_ID = National_ID;
            this.password = password;
            this.address = address;
            ProfilePicture = image;
        }
        public static bool check_email(string input)
        {
            string strRegex = @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z";
            Regex re = new Regex(strRegex, RegexOptions.IgnoreCase);
            if (re.IsMatch(input))
                return true;
            else
                return false;
        }
        public static bool check_PhoneNumber(string input)
        {
            string r1 = @"([09]{2}[0-9]{9})", r2 = @"([9]{1}[0-9]{9})", r3 = @"([+9809]{5}[0-9]{9})", r4 = @"([00989]{5}[0-9]{9})";
            Regex Re1 = new Regex(r1), Re2 = new Regex(r2), Re3 = new Regex(r3), Re4 = new Regex(r4);
            if (Re1.IsMatch(input) || Re2.IsMatch(input) || Re3.IsMatch(input) || Re4.IsMatch(input))
                return true;
            else
                return false;
        }
        public static bool check_Password(string input)
        {
            string strRegex = @"^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#.?!@$%^&*-]).{3,}$";
            Regex re = new Regex(strRegex, RegexOptions.IgnoreCase);
            if (re.IsMatch(input))
                return true;
            else
                return false;
        }
        public static void SaveToCustomersFile(Customer customer)
        {
            string[] line = File.ReadAllLines("Customers.csv");
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < line.Length; i++)
                sb.Append(line[i] + "\n");
            sb.Append(customer.FirstName + "," + customer.LastName + "," + customer.email + "," + customer.password + "," + customer.phoneNumber + "," + customer.National_ID + "," + customer.address + "\n");
            File.WriteAllText("Customers.csv", sb.ToString());
        }
    }
}
