﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;

namespace project
{
    public partial class Admin_Profile : Window
    {
        string FirstName, LastName, Email, User_ID, Password, PhoneNumber;
        Image photo;
        public Admin_Profile(string firstname, string lastname)
        {
            InitializeComponent();
            FirstName = firstname; LastName = lastname;
            var file = File.ReadAllLines("Admins.csv");
            firstname_box.Text = FirstName;
            Lastname_box.Text = LastName;
            foreach (string line in file)
            {
                var data = line.Split(',');
                if (data[0] == firstname && data[1] == lastname)
                {
                    Email = data[2]; User_ID = data[3]; Password = data[4]; PhoneNumber = data[5];
                    email.Text = Email;
                    user_ID.Text = User_ID;
                    pass.Text = Password;
                    phone_number.Text = PhoneNumber;
                }
            }
            string directory = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
            string filePath = Path.Combine(directory, @"C:C:\Users\TTM\Desktop\final project\project\project\bin\Debug\Admins\" + FirstName + " " + LastName + ".jpg");
            if (File.Exists(filePath))
            {
                BitmapImage image = new BitmapImage();
                image.BeginInit();
                image.UriSource = new Uri(@"C:\Users\TTM\Desktop\final project\project\project\bin\Debug\Admins\" + FirstName + " " + LastName + ".jpg");
                image.EndInit();
                ProfilePhoto.Source = image;
            }
        }
        private void ChangeProfile_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog op = new OpenFileDialog();
            op.Title = "Select a picture";
            op.Filter = "All supported graphics|*.jpg;*.jpeg;*.png|" + "JPEG (*.jpg;*.jpeg)|*.jpg;*.jpeg|" + "Portable Network Graphic (*.png)|*.png";
            if (op.ShowDialog() == true)
            {
                ProfilePhoto.Source = new BitmapImage(new Uri(op.FileName));
                photo = ProfilePhoto;
            }
        }
        private void SavePhoto_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog f = new SaveFileDialog();
            string name = FirstName + "." + LastName + ".jpg";
            f.Filter = "All supported graphics|*.jpg;*.jpeg;*.png|" + "JPEG (*.jpg;*.jpeg)|*.jpg;*.jpeg|" + "Portable Network Graphic (*.png)|*.png";
            var encoder = new PngBitmapEncoder();
            encoder.Frames.Add(BitmapFrame.Create((BitmapSource)ProfilePhoto.Source));
            using (FileStream stream = new FileStream(@"C:\Users\TTM\Desktop\final project\project\project\bin\Debug\Admins\"+name, FileMode.Create))
            { encoder.Save(stream); stream.Close(); }
        }
        private void apply_changes_Click(object sender, RoutedEventArgs e)
        {
            if (!user_ID.Text.Contains("admin"))
                MessageBox.Show("username must contain (admin)", "Alert", MessageBoxButton.OK, MessageBoxImage.Error);
            else if (!check_email(email.Text))
                MessageBox.Show("Invalid Email !", "Alert", MessageBoxButton.OK, MessageBoxImage.Error);
            else if (!check_PhoneNumber(phone_number.Text))
                MessageBox.Show("Invalid phone number !", "Alert", MessageBoxButton.OK, MessageBoxImage.Error);
            else
            {
                List<string> lines = new List<string>();
                using (StreamReader reader = new StreamReader("Admins.csv"))
                {
                    string line;
                    while ((line = reader.ReadLine()) != null)
                    {
                        var data = line.Split(',');
                        if (data[0] == FirstName && data[1] == LastName)
                        {
                            data[0] = firstname_box.Text; FirstName = firstname_box.Text;
                            data[1] = Lastname_box.Text; LastName = Lastname_box.Text;
                            data[2] = email.Text;
                            data[3] = user_ID.Text;
                            data[4] = pass.Text;
                            data[5] = phone_number.Text;
                            line = string.Join(",", data);
                        }
                        lines.Add(line);
                    }
                    reader.Close();
                }
                using (StreamWriter writer = new StreamWriter("Admins.csv", false))
                {
                    foreach (string line in lines)
                        writer.WriteLine(line);
                    writer.Close();
                }
                applied.Visibility = Visibility.Visible;
            }
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Hide(); new MainWindow().Show(); Close();
        }
        public static bool check_email(string input)
        {
            string strRegex = @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z";
            Regex re = new Regex(strRegex, RegexOptions.IgnoreCase);
            if (re.IsMatch(input))
                return true;
            else
                return false;
        }
        public static bool check_PhoneNumber(string input)
        {
            string r1 = @"([09]{2}[0-9]{9})", r2 = @"([9]{1}[0-9]{9})", r3 = @"([+9809]{5}[0-9]{9})", r4 = @"([00989]{5}[0-9]{9})";
            Regex Re1 = new Regex(r1), Re2 = new Regex(r2), Re3 = new Regex(r3), Re4 = new Regex(r4);
            if (Re1.IsMatch(input) || Re2.IsMatch(input) || Re3.IsMatch(input) || Re4.IsMatch(input))
                return true;
            else
                return false;
        }
    }
}
