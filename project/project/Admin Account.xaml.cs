﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;

namespace project
{
    public partial class Admin_Account : Window
    {
        string FirstName, LastName, email;
        Image menu;
        public Admin_Account(string name, string lastname, string email)
        {
            InitializeComponent();
            FirstName = name;
            LastName = lastname;
            Admin_name.Text = "Dear " + name + " " + lastname;
            this.email = email;
            Admin_email.Text = email;
            var file = File.ReadAllLines("orders.csv");
            StringBuilder sb = new StringBuilder();
            foreach (string line in file)
            {
                var data = line.Split(',');
                sb.Append(data[0] + "\n");
                for (int i = 2; i < data.Length; i += 2)
                    sb.Append(data[i] + "(" + data[i + 1] + ")/");
                sb.Append("\n--> " + data[1]);
                sb.Append("\n");
            }
            orders.Text = sb.ToString();
            List<string> lines = new List<string>();
            using (StreamReader reader = new StreamReader("Admins.csv"))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    if (line.Contains(","))
                    {
                        var data = line.Split(',');
                        if (data[0] == FirstName && data[1] == LastName && data[2] == email)
                        {
                            data[6] = (int.Parse(data[6])+1).ToString();
                            StringBuilder new_pass = new StringBuilder();
                            int yek = int.Parse(data[6]) % 10;
                            char[] username = data[3].ToCharArray();
                            int sefr = 0;
                            foreach (char ch in username)
                                if (ch == 'a' || ch == 'e' || ch == 'o' || ch == 'i' || ch == 'u')
                                    sefr++;
                            for (int i = 0; i < yek; i++)
                                new_pass.Append("1");
                            for (int i = 0; i < sefr; i++)
                                new_pass.Append("0");
                            data[4] = new_pass.ToString();
                            line = string.Join(",", data);
                        }
                    }
                    lines.Add(line);
                }
                reader.Close();
            }
            using (StreamWriter writer = new StreamWriter("Admins.csv", false))
            {
                foreach (string line in lines)
                    writer.WriteLine(line);
                writer.Close();
            }
            if (File.Exists("economics.txt"))
                economics.Text = File.ReadAllText("economics.txt");
            else
                economics.Text = "No one has paid yet";
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Hide(); new MainWindow().Show(); Close();
        }
        private void ApplyChanges_Click(object sender, RoutedEventArgs e)
        {
            List<string> lines = new List<string>();
            using (StreamReader reader = new StreamReader("Foods_inventory.csv"))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    if (line.Contains(","))
                    {
                        var data = line.Split(',');
                        if (data[0]==foods[lines.Count] || data[3]==prices[lines.Count] || data[0]==FoodNames[lines.Count].Text ||data[3]==Contents[lines.Count].Text)
                        {
                            data[0] = FoodNames[lines.Count].Text;
                            data[1] = Prices[lines.Count].Text;
                            data[2] = textBoxes[lines.Count].Text;
                            data[3] = Contents[lines.Count].Text;
                            line = string.Join(",", data);
                        }
                    }
                    lines.Add(line);
                }
                reader.Close();
            }
            using (StreamWriter writer = new StreamWriter("Foods_inventory.csv", false))
            {
                foreach (string line in lines)
                    writer.WriteLine(line);
                writer.Close();
            }
        }
        private void AddItem_Click(object sender, RoutedEventArgs e)
        {
            if (FoodName.Text == "" || FoodPrice.Text == "" || FoodInventory.Text == "" || FoodContents.Text == "")
            { MessageBox.Show("fill all the necessary blanks"); return; }
            string type = null;
            if (FoodType.SelectedItem == Starter) type = "STARTERS";
            else if (FoodType.SelectedItem == Burger) type = "BURGERS";
            else if (FoodType.SelectedItem == Beverage) type = "BEVERAGES";
            else
            { MessageBox.Show("select the item's type"); return; }
            StringBuilder sb = new StringBuilder();
            double price = double.Parse(FoodPrice.Text);
            price = price + (price*0.24);
            sb.Append(FoodName.Text + "," + price.ToString() + "$," + FoodInventory.Text + "," + FoodContents.Text + "," + type + "\n");
            File.AppendAllText("Foods_inventory.csv", sb.ToString());
            if (NewFoodImage.Source!=null)
            {
                SaveFileDialog f = new SaveFileDialog();
                f.Filter = "All supported graphics|*.jpg;*.jpeg;*.png|" + "JPEG (*.jpg;*.jpeg)|*.jpg;*.jpeg|" + "Portable Network Graphic (*.png)|*.png";
                var encoder = new PngBitmapEncoder();
                encoder.Frames.Add(BitmapFrame.Create((BitmapSource)NewFoodImage.Source));
                using (FileStream stream = new FileStream(@"C:\Users\TTM\Desktop\final project\project\project\bin\Debug\Foods\" + FoodName.Text + ".jpg", FileMode.Create))
                { encoder.Save(stream); stream.Close(); }
            }
        }
        private void UploadMenu_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog op = new OpenFileDialog();
            op.Title = "Select a picture";
            op.Filter = "All supported graphics|*.jpg;*.jpeg;*.png|" + "JPEG (*.jpg;*.jpeg)|*.jpg;*.jpeg|" + "Portable Network Graphic (*.png)|*.png";
            if (op.ShowDialog() == true)
                MenuPicture.Source = new BitmapImage(new Uri(op.FileName));
        }
        private void SaveMenu_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog f = new SaveFileDialog();
            f.Filter = "All supported graphics|*.jpg;*.jpeg;*.png|" + "JPEG (*.jpg;*.jpeg)|*.jpg;*.jpeg|" + "Portable Network Graphic (*.png)|*.png";
            var encoder = new PngBitmapEncoder();
            encoder.Frames.Add(BitmapFrame.Create((BitmapSource)menu.Source));
            using (FileStream stream = new FileStream("Menu.png", FileMode.Create))
            { encoder.Save(stream); stream.Close(); }

        }
        private void Admin_Profile_Click(object sender, RoutedEventArgs e)
        {
            Hide(); new Admin_Profile(FirstName, LastName).Show(); Close();
        }
        private void OrderType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (OrderType.SelectedItem == AllOrders)
            {
                var file = File.ReadAllLines("orders.csv");
                StringBuilder sb = new StringBuilder();
                foreach (string line in file)
                {
                    var data = line.Split(',');
                    sb.Append(data[0] + "\n");
                    for (int i = 2; i < data.Length; i += 2)
                        sb.Append(data[i] + "(" + data[i + 1] + ")/");
                    sb.Append("\n--> " + data[1]);
                    sb.Append("\n");
                }
                orders.Text = sb.ToString();
            }
            else if (OrderType.SelectedItem == PaidOrders)
            {
                var file = File.ReadAllLines("orders.csv");
                StringBuilder sb = new StringBuilder();
                foreach (string line in file)
                {
                    var data = line.Split(',');
                    if (data[0].Contains("paid"))
                    {
                        sb.Append(data[0] + "\n");
                        for (int i = 2; i < data.Length; i += 2)
                            sb.Append(data[i] + "(" + data[i + 1] + ")/");
                        sb.Append("\n--> " + data[1]);
                        sb.Append("\n");
                    }
                }
                orders.Text = sb.ToString();
            }
            else if (OrderType.SelectedItem == NotPaidOrders)
            {
                var file = File.ReadAllLines("orders.csv");
                StringBuilder sb = new StringBuilder();
                foreach (string line in file)
                {
                    var data = line.Split(',');
                    if (data[0].Contains("not paid"))
                    {
                        sb.Append(data[0] + "\n");
                        for (int i = 2; i < data.Length; i += 2)
                            sb.Append(data[i] + "(" + data[i + 1] + ")/");
                        sb.Append("\n--> " + data[1]);
                        sb.Append("\n");
                    }
                }
                orders.Text = sb.ToString();
            }
        }
        private void SaveAddress_Click(object sender, RoutedEventArgs e)
        {
            StreamWriter writer = new StreamWriter("Restaurant_Address.txt");
            writer.WriteLine(addess.Text);
            writer.Close();
        }
        private void ExplorePhoto_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog op = new OpenFileDialog();
            op.Title = "Select a picture";
            op.Filter = "All supported graphics|*.jpg;*.jpeg;*.png|" + "JPEG (*.jpg;*.jpeg)|*.jpg;*.jpeg|" + "Portable Network Graphic (*.png)|*.png";
            if (op.ShowDialog() == true)
            {
                NewFoodImage.Source = new BitmapImage(new Uri(op.FileName));
                menu = MenuPicture;
            }
        }
        private void CancelOrders_Click(object sender, RoutedEventArgs e)
        {
            Hide(); new Cancel_Order("Admin").Show(); Close();
        }
        private void ShowInventories_Click(object sender, RoutedEventArgs e)
        {
            LoadFoodsFromFile();
            Grid grid = new Grid();
            Button button1 = new Button(); button1.Content = "apply changes"; button1.Margin = new Thickness(50, 20, 200, 20); button1.Name = "ApplyChanges";
            button1.Click += new RoutedEventHandler(ApplyChanges_Click);
            Button button2 = new Button(); button2.Content = "reset all changes"; button2.Margin = new Thickness(200, 20, 50, 20); button2.Name = "ResetAllChanges";
            button2.Click += new RoutedEventHandler(ResetAllChanges);
            grid.Children.Add(button1); grid.Children.Add(button2);
            panel.Children.Add(grid);
        }
        private void ResetAllChanges(object sender, RoutedEventArgs e)
        {
            for(int i=0; i<numbers.Count; i++)
            {
                textBoxes[i].Text = numbers[i].ToString();
                FoodNames[i].Text = foods[i];
                Prices[i].Text = prices[i];
                Contents[i].Text = contents[i];
            }
        }
        List<TextBox> FoodNames = new List<TextBox>();
        List<TextBox> textBoxes = new List<TextBox>();
        List<TextBox> Prices = new List<TextBox>();
        List<TextBox> Contents = new List<TextBox>();
        List<int> numbers = new List<int>();
        List<string> foods = new List<string>();
        List<string> prices = new List<string>();
        List<string> contents = new List<string>();
        public void LoadFoodsFromFile()
        {
            var file = File.ReadAllLines("Foods_inventory.csv");
            foreach (string line in file)
            {
                Grid grid = new Grid();
                TextBox Food = new TextBox(); Food.Margin = new Thickness(20, 0, 225, 0);
                TextBox Inventory = new TextBox(); Inventory.Margin = new Thickness(150, 0, 190, 0);
                TextBox price = new TextBox(); price.Margin = new Thickness(186, 0, 150, 0);
                TextBox content = new TextBox(); content.Margin = new Thickness(226, 0, 20, 0);
                var data = line.Split(',');
                Food.Text = data[0].ToString(); Inventory.Text = data[2].ToString(); price.Text = data[1]; content.Text = data[3];
                numbers.Add(int.Parse(data[2])); foods.Add(data[0]); prices.Add(data[1]); contents.Add(data[3]);
                FoodNames.Add(Food); textBoxes.Add(Inventory); Prices.Add(price); Contents.Add(content);
                grid.Children.Add(Food); grid.Children.Add(Inventory); grid.Children.Add(price); grid.Children.Add(content);
                panel.Children.Add(grid);
            }
        }
    }
}
