﻿using System;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;

namespace project
{
    public partial class Previous_orders : Window
    {
        string FirstName, LastName;
        public Previous_orders(string firstname, string lastname)
        {
            InitializeComponent();
            FirstName = firstname;
            LastName = lastname;
            string[] allFiles = Directory.GetFiles(@"C:\Users\TTM\Desktop\final project\project\project\bin\Debug\Bills");
            foreach (string file in allFiles)
            {
                if (file.Contains(FirstName)&&file.Contains(LastName))
                {
                    Image bill = new Image();
                    bill.Height = 500;
                    BitmapImage image = new BitmapImage();
                    image.BeginInit();
                    image.UriSource = new Uri(file);
                    image.EndInit();
                    bill.Source = image;
                    panel.Children.Add(bill);
                }
            }
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            new MainWindow().Show(); Close();
        }
    }
}
