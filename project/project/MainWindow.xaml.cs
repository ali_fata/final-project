﻿using System.IO;
using System.Text;
using System.Windows;

namespace project
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            if (!File.Exists("Foods_inventory.csv"))
            {
                StringBuilder s1 = new StringBuilder();
                s1.Append("chicken panini,5$,10,bacon-cheese-bread-butter-pesto,BURGERS\nsandwich,4$,10,ham-tomato-bread-salad leaves,BURGERS\nclassic cheese burger,2$,10,Beef Patty-Cheese-Ketchup-Onions,BURGERS\nhot dog,5$,10,pork-beef-paprika-pepper,BURGERS\n");
                s1.Append("chips and salsa,5$,10,tomato-olive oil-onion-lime juice-cilantro,STARTERS\nchicken wings,4$,10,chicken wings-hot sauce-garlic powder-pepper,STARTERS\nchicken nuggets,6$,10,chicken breasts-egg-granulated garlic-black pepper,STARTERS\nlarge french fries,2$,10,russet potatoes-Ketchup,STARTERS\n");
                s1.Append("mozzarella sticks,2$,10,milk mozzarella cheese-egg-breadcrumbs,STARTERS\npopcorn,2$,10,corn-oil-salt,STARTERS\nsmoothies,5$,10,banana-strawberries-milk-vanilla yogurt,BEVERAGES\namerican coffee,4$,10,coffee beans-boiling water,BEVERAGES\n");
                s1.Append("espresso,2$,10,coffee liqueur-sugar-vodka,BEVERAGES\ncuppussino,2$,10,milk-coffee-cocoa powder,BEVERAGES\nlatte,2$,10,roast espresso coffee-milk,BEVERAGES\nhot tea,5$,10,Caffeine-cloves-cinnamon,BEVERAGES\n");
                File.WriteAllText("Foods_inventory.csv", s1.ToString());
            }
            if (!File.Exists("Admins.csv"))
            {
                StringBuilder s2 = new StringBuilder();
                s2.Append("First Name,Last Name,Email,user ID,Password,phone number,login\n");
                s2.Append("John,Adams,JohnAdams@gmail.com,admin123,00,09364659264,0\n");
                s2.Append("Peter,Lee,PeterLee@gmail.com,admin456,00,09123793755,0\n");
                s2.Append("Billy,Smith,BillySmith@gmail.com,admin789,00,09024163749,0\n");
                File.WriteAllText("Admins.csv", s2.ToString());
            }
            if (!File.Exists("Customers.csv"))
            {
                StringBuilder s3 = new StringBuilder();
                s3.Append("First Name, Last Name, Email, Password, phone number, national ID, Address\n");
                File.AppendAllText("Customers.csv", s3.ToString());
            }
        }
        private void Adminbtn_Click(object sender, RoutedEventArgs e)
        {
            Hide();
            new Login_Admin().Show();
            Close();
        }
        private void Customerbtn_Click(object sender, RoutedEventArgs e)
        {
            Hide();
            (new Login_Customer()).Show();
            Close();
        }
    }
}
