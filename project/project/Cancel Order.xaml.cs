﻿using System.Collections.Generic;
using System.IO;
using System.Windows;
using System.Windows.Media;

namespace project
{
    public partial class Cancel_Order : Window
    {
        string Type;
        public Cancel_Order(string Type)
        {
            InitializeComponent();
            this.Type = Type;
        }
        private void BackToMain_Click(object sender, RoutedEventArgs e)
        {
            Hide(); new MainWindow().Show(); Close();
        }
        private void ok_Click(object sender, RoutedEventArgs e)
        {
            bool Find = false; bool paid = false; bool online = false;
            List<string> lines = new List<string>();
            using (StreamReader reader = new StreamReader("orders.csv"))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    if (line.Contains(","))
                    {
                        var data = line.Split(',');
                        if (data[0].Contains(TrackingNumberBlock.Text))
                        {
                            Find = true;
                            if (data[0].Contains("paid") && data[0].Contains("OnlinePayment"))
                            { paid = true; online = true; }
                            else if (data[0].Contains("paid") && data[0].Contains("AttendecePayment"))
                            { paid = true; online = false; }
                            else if (data[0].Contains("not paid"))
                                paid = false;
                        }
                        else
                        {
                            line = string.Join(",", data);
                            lines.Add(line);
                        }
                    }   
                }
                reader.Close();
            }
            using (StreamWriter writer = new StreamWriter("orders.csv", false))
            {
                foreach (string line in lines)
                    writer.WriteLine(line);
                writer.Close();
            }
            if (Type == "Customer")
            {
                if (Find)
                {
                    if (paid)
                    {
                        if (online)
                            DoneOrFail.Text = "Your order has been canceled and 90% of the total cost returned to you and 10% was deducted as tax.";
                        if (!online)
                            DoneOrFail.Text = "Your order has been canceled. Please pay 10% of the total amount as tax.";
                    }
                    else if (!paid)
                    { DoneOrFail.Text = "Your order has been canceled successfully."; }
                    DoneOrFail.Foreground = new SolidColorBrush(Colors.Green);
                }
                else
                { DoneOrFail.Text = "The tracking number was not found! try again"; DoneOrFail.Foreground = new SolidColorBrush(Colors.Red); }
            }
            else if (Type == "Admin")
            {
                if (Find)
                {
                    if (paid)
                    {
                        if (online)
                            DoneOrFail.Text = "The order has been canceled and all of the cost returned to the customer.";
                        if (!online)
                            DoneOrFail.Text = "Your order has been canceled successfully.";
                    }
                    else if (!paid)
                    { DoneOrFail.Text = "Your order has been canceled successfully."; }
                    DoneOrFail.Foreground = new SolidColorBrush(Colors.Green);
                }
                else
                { DoneOrFail.Text = "The tracking number was not found! try again"; DoneOrFail.Foreground = new SolidColorBrush(Colors.Red); }
            }
        }
    }
}
