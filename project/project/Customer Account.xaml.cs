﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace project
{
    public static class ExtensionMethods
    {
        public static IEnumerable<DependencyObject> Descendents(this DependencyObject root)
        {
            int count = VisualTreeHelper.GetChildrenCount(root);
            for (int i = 0; i < count; i++)
            {
                var child = VisualTreeHelper.GetChild(root, i);
                yield return child;
                foreach (var descendent in Descendents(child))
                    yield return descendent;
            }
        }
    }
    public partial class Customer_Account : Window
    {
        string FirstName, LastName;
        List<double> prices = new List<double>();
        List<int> inventories = new List<int>();
        List<string> contents = new List<string>();
        public Customer_Account(string name, string lastname)
        {
            InitializeComponent();
            FirstName = name;
            LastName = lastname;
            cunstomer_name.Text = "Dear " + name + " " + lastname;
            OrderCalender.DisplayDateStart = DateTime.Today;
            DiscountCode.Text = GenerateDiscountCode();
            list.Clear();
            var file = File.ReadAllLines("Foods_inventory.csv");
            foreach (string line in file)
            {
                var data = line.Split(',');
                char[] chars = data[1].ToCharArray();
                prices.Add(chars[0] - '0');
                inventories.Add(int.Parse(data[2]));
                contents.Add(data[3]);
                Foods.Items.Add(data[0]+" ("+data[1]+")");
            }
            foreach (string str in Foods.Items)
            {
                list.Add(str);
                Search.CharacterCasing = CharacterCasing.Lower;
            }
            LoadFoodsFromFile(file);
            Button shop = new Button(); shop.Content = "Add to shopping basket"; shop.Margin = new Thickness(70,10,70,40);
            shop.Click += new RoutedEventHandler(ShopButton_Click);
            Customer_Panel.Children.Add(shop);
        }
        private void ConfirmDate_Click(object sender, RoutedEventArgs e)
        {
            SelectedCalenderDate.Text = OrderCalender.SelectedDate.Value.ToString();
        }
        List<CheckBox> checkBoxes = new List<CheckBox>();
        List<TextBox> Numbers = new List<TextBox>();
        List<TextBlock> Inventories = new List<TextBlock>();
        List<Button> Infos = new List<Button>();
        public void LoadFoodsFromFile(string[] file)
        {
            if (TypeFilter.SelectedItem == All)
            {
                foreach (string line in file)
                {
                    Grid grid = new Grid();
                    CheckBox box = new CheckBox();
                    TextBox textBox = new TextBox();
                    TextBlock invertory = new TextBlock();
                    TextBlock price = new TextBlock();
                    Button button = new Button();
                    var data = line.Split(',');
                    box.Content = data[0]; textBox.Text = "0"; invertory.Text = data[2]; price.Text = data[1]; button.Content = "...";
                    Thickness margin1 = box.Margin; margin1.Left = 10; margin1.Right = 220; margin1.Top = 0; box.Margin = margin1;
                    Thickness margin2 = box.Margin; margin2.Left = 170; margin2.Right = 180; margin2.Top = 0; textBox.Margin = margin2;
                    Thickness margin3 = box.Margin; margin3.Left = 220; margin3.Right = 0; margin3.Top = 0; invertory.Margin = margin3;
                    Thickness margin4 = box.Margin; margin4.Left = 275; margin4.Right = 0; margin4.Top = 0; price.Margin = margin4;
                    Thickness margin5 = box.Margin; margin5.Left = 320; margin5.Right = 30; margin5.Top = 0; button.Margin = margin5;
                    string type = data[4];
                    button.Name = "btn" + checkBoxes.Count;
                    button.Click += new RoutedEventHandler(InfoClick); box.Checked += new RoutedEventHandler(checked_item);
                    grid.Name = type + checkBoxes.Count;
                    checkBoxes.Add(box); Numbers.Add(textBox); Inventories.Add(invertory); Infos.Add(button);
                    grid.Children.Add(box); grid.Children.Add(textBox); grid.Children.Add(invertory); grid.Children.Add(price); grid.Children.Add(button);
                    grid.Height = 16;
                    Customer_Panel.Children.Add(grid);
                }
            }
        }
        private void TypeFilter_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var file = File.ReadAllLines("Foods_inventory.csv");
            if (TypeFilter.SelectedItem == All)
            {
                foreach (var a in this.Descendents().OfType<Grid>())
                {
                    Grid n = a as Grid;
                    n.Visibility = Visibility.Visible;
                }
            }
            if (TypeFilter.SelectedItem == Burgers)
            {
                foreach (var a in this.Descendents().OfType<Grid>())
                {
                    Grid n = a as Grid;
                    if (n.Name.Contains("STARTERS") || n.Name.Contains("BEVERAGES"))
                        n.Visibility = Visibility.Hidden;
                    else
                        n.Visibility = Visibility.Visible;
                }
            }
            if (TypeFilter.SelectedItem == Starters)
            {
                foreach (var a in this.Descendents().OfType<Grid>())
                {
                    Grid n = a as Grid;
                    if (n.Name.Contains("BURGERS") || n.Name.Contains("BEVERAGES"))
                        n.Visibility = Visibility.Hidden;
                    else
                        n.Visibility = Visibility.Visible;
                }
            }
            if (TypeFilter.SelectedItem == Beveragers)
            {
                foreach (var a in this.Descendents().OfType<Grid>())
                {
                    Grid n = a as Grid;
                    if (n.Name.Contains("STARTERS") || n.Name.Contains("BURGERS"))
                        n.Visibility = Visibility.Hidden;
                    else
                        n.Visibility = Visibility.Visible;
                }
            }
        }
        private void checked_item(object sender, EventArgs e)
        {
            CheckBox clicked = (CheckBox)sender;
            int n = 0;
            for (int i= 0; i < checkBoxes.Count; i++)
                if (checkBoxes[i].Content.ToString() == clicked.Content.ToString())
                    n = i;
            Numbers[n].Text = "1";
        }
        private void InfoClick(object sender, EventArgs e)
        {
            Button clicked = (Button)sender;
            char[] chars = clicked.Name.ToCharArray();
            string i;
            if (chars.Length == 4)
                i = chars[3].ToString();
            else
                i = chars[3].ToString() + chars[4].ToString();
            var file = File.ReadAllLines("Foods_inventory.csv");
            foreach (string line in file)
            {
                var data = line.Split(',');
                if (data[0] == checkBoxes[int.Parse(i)].Content.ToString())
                {
                    FoodContents.Text = data[3];
                    Type.Text = data[4];
                }
            }
            string directory = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
            string filePath = Path.Combine(directory, @"C:\Users\TTM\Desktop\final project\project\project\bin\Debug\Foods\" + checkBoxes[int.Parse(i)].Content.ToString() + ".jpg");
            if (File.Exists(filePath))
            {
                BitmapImage image = new BitmapImage();
                image.BeginInit();
                image.UriSource = new Uri(@"C:\Users\TTM\Desktop\final project\project\project\bin\Debug\Foods\" + checkBoxes[int.Parse(i)].Content.ToString() + ".jpg");
                image.EndInit();
                FoodImage.Source = image;
            }
            ContentsGrid.Visibility = Visibility.Visible;
        }
        private void ShopButton_Click(object sender, RoutedEventArgs e)
        {
            bool available = true;
            var numbers = "0123456789";
            var random = new Random();
            var result = new string(Enumerable.Repeat(numbers, 10).Select(s => s[random.Next(s.Length)]).ToArray());
            string TrackingNumber = result.ToString();
            for (int i = 0; i < inventories.Count; i++)
            {
                if (inventories[i] < int.Parse(Numbers[i].Text))
                    available = false;
            }
            if (!available)
                MessageBox.Show("Pay attention to inventories, some of your orders are unavailable");
            else
            {
                double cost = 0;
                for (int i = 0; i < prices.Count; i++)
                {
                    double p = double.Parse(Numbers[i].Text) * prices[i];
                    cost += p;
                }
                DateTime submitDate = DateTime.Now;
                StringBuilder sb = new StringBuilder();
                sb.Append(FirstName + " " + LastName + " (not paid)-" + TrackingNumber + "," + SelectedCalenderDate.Text.ToString());
                for(int i = 0; i < Numbers.Count; i++)
                {
                    if (Numbers[i].Text != 0.ToString())
                    {
                        sb.Append("," + checkBoxes[i].Content.ToString() + "," + Numbers[i].Text);
                    }
                }
                sb.Append("\n");
                File.AppendAllText("orders.csv", sb.ToString());
                List<string> lines = new List<string>();
                using (StreamReader reader = new StreamReader("Foods_inventory.csv"))
                {
                    string line;
                    while ((line = reader.ReadLine()) != null)
                    {
                        if (line.Contains(","))
                        {
                            var data = line.Split(',');
                            int number = int.Parse(data[2]);
                            number = number - int.Parse(Numbers[lines.Count].Text);
                            data[2] = number.ToString();
                            line = string.Join(",", data);
                        }
                        lines.Add(line);
                    }
                    reader.Close();
                }
                using (StreamWriter writer = new StreamWriter("Foods_inventory.csv", false))
                {
                    foreach (string line in lines)
                        writer.WriteLine(line);
                    writer.Close();
                }
                Hide(); new signature(FirstName, LastName, cost, submitDate.ToString(), SelectedCalenderDate.Text, inventories, TrackingNumber, DiscountCode.Text).Show(); Close();
            }
        }
        private void Profile_Click(object sender, RoutedEventArgs e)
        {
            Hide(); new Customer_Profile(FirstName, LastName).Show(); Close();
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Hide(); new MainWindow().Show(); Close();
        }
        private void PreviousOrders_Click(object sender, RoutedEventArgs e)
        {
            Hide(); new Previous_orders(FirstName,LastName).Show(); Close();
        }
        private void CancelPreviousOrders_Click(object sender, RoutedEventArgs e)
        {
            Hide(); new Cancel_Order("Customer").Show(); Close();
        }
        List<string> list = new List<string>();
        private void Search_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (string.IsNullOrEmpty(Search.Text) == false)
            {
                Foods.Items.Clear();
                foreach (string str in list)
                    if (str.StartsWith(Search.Text) || str.Contains(Search.Text))
                        Foods.Items.Add(str);
            }
            else if (Search.Text == "")
            {
                Foods.Items.Clear();
                foreach (string str in list)
                    Foods.Items.Add(str);
            }
        }
        public string GenerateDiscountCode()
        {
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            var random = new Random();
            var result = new string(Enumerable.Repeat(chars, 8).Select(s => s[random.Next(s.Length)]).ToArray());
            return result.ToString();
        }
    }
}
