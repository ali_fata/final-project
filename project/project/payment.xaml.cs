﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace project
{
    public partial class payment : Window
    {
        string Name, Lastname, SubmitDate, DeliveyDate, TrackingNumber, discount_code;
        double Cost, PaidCost;
        List<string> orders = new List<string>();
        List<int> numbers = new List<int>();
        List<double> costs = new List<double>();
        List<int> last_inventories = new List<int>();
        int HowManyTimesToOrder = 0;
        public payment(string name, string lastname, double cost, string SubmitDate, string DeliveyDate, List<int> inventories, string TrackingNumber, string discount)
        {
            InitializeComponent();
            Name = name; Lastname = lastname; Cost = cost;
            this.SubmitDate = SubmitDate;
            this.DeliveyDate = DeliveyDate;
            this.TrackingNumber = TrackingNumber;
            last_inventories = inventories;
            discount_code = discount;
            NameBlock.Text = Name + " " + Lastname;
            SubDate.Text = SubmitDate;
            DelDate.Text = DeliveyDate;
            TrackingNumberBlock.Text = TrackingNumber;
            var file = File.ReadAllLines("orders.csv");
            var Foods = File.ReadAllLines("Foods_inventory.csv");
            StringBuilder s1 = new StringBuilder(), s2 = new StringBuilder(), s3 = new StringBuilder();
            foreach (string line in file)
            {
                var data = line.Split(',');
                if (data[0].Contains(Name) && data[0].Contains(Lastname) && data[0].Contains("not paid") && data[1] == DeliveyDate)
                {
                    for (int i = 2; i < data.Length; i += 2)
                    {
                        orders.Add(data[i]); numbers.Add(int.Parse(data[i + 1]));
                        double price=0;
                        foreach (string food in Foods)
                        {
                            var item = food.Split(',');
                            if (item[0] == data[i])
                            {
                                char[] chars = item[1].ToCharArray();
                                price = double.Parse(data[i+1]) * (chars[0]-'0');
                            }
                        }
                        costs.Add(price);
                    }
                }
                if(data[0].Contains(Name) && data[0].Contains(Lastname))
                    HowManyTimesToOrder++;
            }
            HowManyTimes.Text = HowManyTimesToOrder.ToString() + "th";
            if (HowManyTimesToOrder <= 4)
            { Tax.Text = 9 + "%"; Discount.Text = 0 + "%"; PaidCost = cost+(cost*0.09); }
            else if (HowManyTimesToOrder > 4 && HowManyTimesToOrder <=8)
            { Tax.Text = 7 + "%"; Discount.Text = 5 + "%"; PaidCost = cost+(cost*0.07)-(cost*0.05); }
            else if (HowManyTimesToOrder > 8 && HowManyTimesToOrder <= 12)
            { Tax.Text = 5 + "%"; Discount.Text = 8 + "%"; PaidCost = cost+(cost*0.05)-(cost*0.08); }
            else if(HowManyTimesToOrder > 12)
            { Tax.Text = 0 + "%"; Discount.Text = 10 + "%"; PaidCost=cost-(cost*0.10); Lottery.Text = "And you will participate in our lottery"; }
            TotalCost.Text = PaidCost + " $";
            for (int i = 0; i < orders.Count; i++)
            {
                s1.Append(orders[i] + "\n");
                s2.Append(numbers[i] + "\n");
                s3.Append(costs[i] + "$\n");
            }
            s3.Append("------\n" + cost + "$");
            OrdersBlock.Text = s1.ToString();
            NumbersBlock.Text = s2.ToString();
            CostsBlock.Text = s3.ToString();
            BitmapImage image = new BitmapImage();
            image.BeginInit();
            image.UriSource = new Uri(@"C:\Users\TTM\Desktop\final project\project\project\bin\Debug\Signatures\"+Name+" "+Lastname+".png");
            image.EndInit();
            Sign.Source = image;
        }
        private void print_Click(object sender, RoutedEventArgs e)
        {
            int Width = (int)this.RenderSize.Width;
            int Height = (int)this.RenderSize.Height;
            print.Visibility = Visibility.Hidden; CancelButton.Visibility = Visibility.Hidden; BackToMain.Visibility = Visibility.Hidden; DiscountTitle.Visibility = Visibility.Hidden; ApplyCode.Visibility = Visibility.Hidden; DiscountCode.Visibility = Visibility.Hidden;
            RenderTargetBitmap renderTargetBitmap = new RenderTargetBitmap(Width, Height, 96, 96, PixelFormats.Pbgra32);
            renderTargetBitmap.Render(this);
            PngBitmapEncoder pngImage = new PngBitmapEncoder();
            pngImage.Frames.Add(BitmapFrame.Create(renderTargetBitmap));
            DateTime date = DateTime.Now; string Date = date.Month.ToString()+"."+date.Day.ToString()+"."+date.Year.ToString()+"-"+date.Hour.ToString()+"."+date.Minute.ToString()+date.Second.ToString();
            using (Stream fileStream = File.Create(@"C:\Users\TTM\Desktop\final project\project\project\bin\Debug\Bills\"+Name+" "+Lastname+" "+Date+".jpg"))
                pngImage.Save(fileStream);
            print.Visibility = Visibility.Visible; DiscountTitle.Visibility = Visibility.Visible; DiscountCode.Visibility = Visibility.Visible; ApplyCode.Visibility = Visibility.Visible; BackToMain.Visibility = Visibility.Visible; CancelButton.Visibility = Visibility.Visible; SaveBill.Visibility = Visibility.Visible; PaymentGrid.Visibility = Visibility.Visible;
        }
        bool paid = false;
        private void PaymentMethod_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            string method = null;
            if (PaymentMethod.SelectedItem == OnlineMethod)
            { MessageBox.Show(PaidCost + "$ deducted from your account\nThank you for your shop"); method = "OnlinePayment"; }
            else if (PaymentMethod.SelectedItem == AttendeceMethod)
            { MessageBox.Show("Thank you for your shop"); method = "AttendecePayment"; }
            paid = true;
            List<string> lines = new List<string>();
            using (StreamReader reader = new StreamReader("orders.csv"))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    if (line.Contains(","))
                    {
                        var data = line.Split(',');
                        if (data[0] == Name + " " + Lastname + " (not paid)-" + TrackingNumber && data[1] == DeliveyDate)
                        {
                            data[0] = Name + " " + Lastname + " (paid|" + method + ")-" + TrackingNumber;
                            line = string.Join(",", data);
                        }
                    }
                    lines.Add(line);
                }
                reader.Close();
            }
            using (StreamWriter writer = new StreamWriter("orders.csv", false))
            {
                foreach (string line in lines)
                    writer.WriteLine(line);
                writer.Close();
            }
            double profit = PaidCost-(Cost-(Cost*0.24));
            string text = null;
            if (File.Exists("economics.txt"))
                text = File.ReadAllText("economics.txt");    
            StreamWriter streamWriter = new StreamWriter("economics.txt");
            streamWriter.WriteLine(text+Name +" "+ Lastname +": submitted at "+ SubmitDate +"\n--> cost: "+ Cost +"$ - paid: "+ PaidCost +"$ - profit: "+ profit +"$");
            streamWriter.Close();
        }
        private void ApplyCode_Click(object sender, RoutedEventArgs e)
        {
            if (DiscountCode.Text == discount_code)
            {
                if (HowManyTimesToOrder == 1)
                {
                    MessageBox.Show("It's your first order, So you got 5% discount");
                    PaidCost -= (PaidCost * 0.05);
                    TotalCost.Text = PaidCost + " $";
                }
                else if (HowManyTimesToOrder == 2)
                {
                    MessageBox.Show("It's your second order, So you got 10% discount");
                    PaidCost -= (PaidCost * 0.10);
                    TotalCost.Text = PaidCost + " $";
                }
                else
                    MessageBox.Show("Sorry, you can use discount code only for your first and second order", "Alert", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else
                MessageBox.Show("Invalid discount code", "Alert", MessageBoxButton.OK, MessageBoxImage.Error);
        }
        private void Canel_Click(object sender, RoutedEventArgs e)
        {
            List<string> lines = new List<string>();
            using (StreamReader reader = new StreamReader("Foods_inventory.csv"))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    if (line.Contains(","))
                    {
                        var data = line.Split(',');
                        data[2] = last_inventories[lines.Count].ToString();
                        line = string.Join(",", data);
                    }
                    lines.Add(line);
                }
                reader.Close();
            }
            using (StreamWriter writer = new StreamWriter("Foods_inventory.csv", false))
            {
                foreach (string line in lines)
                    writer.WriteLine(line);
                writer.Close();
            }
            if (paid && PaymentMethod.SelectedItem == OnlineMethod)
                MessageBox.Show("Your order has been canceled and 90% of the total cost returned to you and 10% was deducted as tax.");
            if (paid && PaymentMethod.SelectedItem == AttendeceMethod)
                MessageBox.Show("Your order has been canceled. Please pay 10% of the total amount as tax");
            Hide(); new MainWindow().Show(); Close();
        }
        private void BackToMain_Click(object sender, RoutedEventArgs e)
        {
            Hide(); new MainWindow().Show(); Close();
        }
    }
}
