﻿using System;
using System.IO;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using project.Properties;

namespace project
{
    public partial class Login_Customer : Window
    {
        public Login_Customer()
        {
            InitializeComponent();
        }
        private void BackToMainMenu_Click(object sender, RoutedEventArgs e)
        {
            Hide();
            (new MainWindow()).Show();
            Close();
        }
        private void CreateAccount_Click(object sender, RoutedEventArgs e)
        {
            Hide();
            (new Create_Customer()).Show();
            Close();
        }
        private void OK_Click(object sender, RoutedEventArgs e)
        {
            bool exist = false, correct_pass = false;
            string[] lines = File.ReadAllLines("Customers.csv");
            string name=null, lastname=null;
            for (int i=0; i<lines.Length; i++)
            {
                string line = lines[i];
                string[] items = line.Split(',');
                if (Customer_email.Text == items[2])
                {
                    exist = true;
                    if (Customer_pass.Text == items[3])
                    {
                        correct_pass = true;
                        name = items[0];
                        lastname = items[1];
                    }
                }
            }
            if (exist && correct_pass)
            {
                MessageBox.Show("You successfully logged in");
                Hide();
                new Customer_Account(name, lastname).Show();
                Close();
            }
            else if (exist && !correct_pass)
                MessageBox.Show("Password is not correct");
            else if (!exist)
                MessageBox.Show("This user doesn't exist");
        }
    }
    public class Customer
    {
        public string FirstName, phoneNumber, LastName, email, National_ID, password, address;
        Image ProfilePicture;
        public Customer(string FirstName, string LastName, string PN, string email, string National_ID, string password, string address)
        {
            this.FirstName = FirstName;             // first cunstructor
            this.LastName = LastName;
            phoneNumber = PN;
            this.email = email;
            this.National_ID = National_ID;
            this.password = password;
            this.address = address;
        }
        public Customer(string FirstName, string LastName, string PN, string email, string National_ID, string password, string address, Image image)
        {
            this.FirstName = FirstName;             // second cunstructor
            this.LastName = LastName;
            phoneNumber = PN;
            this.email = email;
            this.National_ID = National_ID;
            this.password = password;
            this.address = address;
            ProfilePicture = image;
        }
    }
}