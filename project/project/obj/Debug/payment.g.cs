﻿#pragma checksum "..\..\payment.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "A57E226DBB59BE53BB67B5B91D23DE37561BB23A906E72C51CC9B4DB84CAADB3"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using project;


namespace project {
    
    
    /// <summary>
    /// payment
    /// </summary>
    public partial class payment : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 12 "..\..\payment.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button CancelButton;
        
        #line default
        #line hidden
        
        
        #line 13 "..\..\payment.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button BackToMain;
        
        #line default
        #line hidden
        
        
        #line 18 "..\..\payment.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock NameBlock;
        
        #line default
        #line hidden
        
        
        #line 22 "..\..\payment.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock SubDate;
        
        #line default
        #line hidden
        
        
        #line 26 "..\..\payment.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock DelDate;
        
        #line default
        #line hidden
        
        
        #line 34 "..\..\payment.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock OrdersBlock;
        
        #line default
        #line hidden
        
        
        #line 35 "..\..\payment.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock NumbersBlock;
        
        #line default
        #line hidden
        
        
        #line 36 "..\..\payment.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock CostsBlock;
        
        #line default
        #line hidden
        
        
        #line 40 "..\..\payment.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock HowManyTimes;
        
        #line default
        #line hidden
        
        
        #line 44 "..\..\payment.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock Tax;
        
        #line default
        #line hidden
        
        
        #line 45 "..\..\payment.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock Discount;
        
        #line default
        #line hidden
        
        
        #line 47 "..\..\payment.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock Lottery;
        
        #line default
        #line hidden
        
        
        #line 50 "..\..\payment.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock TotalCost;
        
        #line default
        #line hidden
        
        
        #line 54 "..\..\payment.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock DiscountTitle;
        
        #line default
        #line hidden
        
        
        #line 55 "..\..\payment.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox DiscountCode;
        
        #line default
        #line hidden
        
        
        #line 56 "..\..\payment.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button ApplyCode;
        
        #line default
        #line hidden
        
        
        #line 57 "..\..\payment.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image Sign;
        
        #line default
        #line hidden
        
        
        #line 59 "..\..\payment.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock TrackingNumberBlock;
        
        #line default
        #line hidden
        
        
        #line 61 "..\..\payment.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button print;
        
        #line default
        #line hidden
        
        
        #line 62 "..\..\payment.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock SaveBill;
        
        #line default
        #line hidden
        
        
        #line 63 "..\..\payment.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid PaymentGrid;
        
        #line default
        #line hidden
        
        
        #line 65 "..\..\payment.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox PaymentMethod;
        
        #line default
        #line hidden
        
        
        #line 66 "..\..\payment.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBoxItem OnlineMethod;
        
        #line default
        #line hidden
        
        
        #line 67 "..\..\payment.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBoxItem AttendeceMethod;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/project;component/payment.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\payment.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.CancelButton = ((System.Windows.Controls.Button)(target));
            
            #line 12 "..\..\payment.xaml"
            this.CancelButton.Click += new System.Windows.RoutedEventHandler(this.Canel_Click);
            
            #line default
            #line hidden
            return;
            case 2:
            this.BackToMain = ((System.Windows.Controls.Button)(target));
            
            #line 13 "..\..\payment.xaml"
            this.BackToMain.Click += new System.Windows.RoutedEventHandler(this.BackToMain_Click);
            
            #line default
            #line hidden
            return;
            case 3:
            this.NameBlock = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 4:
            this.SubDate = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 5:
            this.DelDate = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 6:
            this.OrdersBlock = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 7:
            this.NumbersBlock = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 8:
            this.CostsBlock = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 9:
            this.HowManyTimes = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 10:
            this.Tax = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 11:
            this.Discount = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 12:
            this.Lottery = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 13:
            this.TotalCost = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 14:
            this.DiscountTitle = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 15:
            this.DiscountCode = ((System.Windows.Controls.TextBox)(target));
            return;
            case 16:
            this.ApplyCode = ((System.Windows.Controls.Button)(target));
            
            #line 56 "..\..\payment.xaml"
            this.ApplyCode.Click += new System.Windows.RoutedEventHandler(this.ApplyCode_Click);
            
            #line default
            #line hidden
            return;
            case 17:
            this.Sign = ((System.Windows.Controls.Image)(target));
            return;
            case 18:
            this.TrackingNumberBlock = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 19:
            this.print = ((System.Windows.Controls.Button)(target));
            
            #line 61 "..\..\payment.xaml"
            this.print.Click += new System.Windows.RoutedEventHandler(this.print_Click);
            
            #line default
            #line hidden
            return;
            case 20:
            this.SaveBill = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 21:
            this.PaymentGrid = ((System.Windows.Controls.Grid)(target));
            return;
            case 22:
            this.PaymentMethod = ((System.Windows.Controls.ComboBox)(target));
            
            #line 65 "..\..\payment.xaml"
            this.PaymentMethod.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.PaymentMethod_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 23:
            this.OnlineMethod = ((System.Windows.Controls.ComboBoxItem)(target));
            return;
            case 24:
            this.AttendeceMethod = ((System.Windows.Controls.ComboBoxItem)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

