﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace project
{
    public partial class signature : Window
    {
        string FirstName, LastName, date1, date2, TrackingNumber, DisCountCode;
        double Cost;
        List<int> last_inventories = new List<int>();
        public signature(string firstname, string lastname, double cost, string submitDate, string deliveryDate, List<int> inventories, string TrackingNumber, string discount)
        {
            InitializeComponent();
            FirstName = firstname; LastName = lastname;
            Cost = cost; date1 = submitDate; date2 = deliveryDate;
            customer.Text = "(Mr./Mrs. " + firstname + " " + lastname + ") :";
            last_inventories = inventories;
            this.TrackingNumber = TrackingNumber;
            DisCountCode = discount;
        }
        Point currentPoint = new Point();
        private void Canvas_MouseDown_1(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (e.ButtonState == MouseButtonState.Pressed)
                currentPoint = e.GetPosition(this);
        }
        private void Canvas_MouseMove_1(object sender, System.Windows.Input.MouseEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                Line line = new Line();
                line.Stroke = SystemColors.WindowFrameBrush;
                line.X1 = currentPoint.X;
                line.Y1 = currentPoint.Y;
                line.X2 = e.GetPosition(this).X;
                line.Y2 = e.GetPosition(this).Y;
                currentPoint = e.GetPosition(this);
                paintSurface.Children.Add(line);
            }
        }
        private void finish_Click(object sender, RoutedEventArgs e)
        {
            RenderTargetBitmap rtb = new RenderTargetBitmap((int)paintSurface.RenderSize.Width,
                (int)paintSurface.RenderSize.Height, 96d, 96d, System.Windows.Media.PixelFormats.Default);
            rtb.Render(paintSurface);
            var crop = new CroppedBitmap(rtb, new Int32Rect(50, 50, 250, 250));
            BitmapEncoder pngEncoder = new PngBitmapEncoder();
            pngEncoder.Frames.Add(BitmapFrame.Create(crop));
            using (var fs = System.IO.File.OpenWrite(@"C:\Users\TTM\Desktop\final project\project\project\bin\Debug\Signatures\" + FirstName + " " + LastName + ".png"))
            {
                pngEncoder.Save(fs);
            }
            saved.Visibility = Visibility.Visible;
            Hide();
            new payment(FirstName, LastName, Cost, date1, date2, last_inventories, TrackingNumber, DisCountCode).Show();
            Close();
        }
    }
}
