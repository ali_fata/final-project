﻿using System.Collections.Generic;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace project
{
    public partial class Login_Admin : Window
    {
        public Login_Admin()
        {
            InitializeComponent();
        }
        private void BackToMain_Click(object sender, RoutedEventArgs e)
        {
            Hide();
            (new MainWindow()).Show();
            Close();
        }
        string name=null, lastname=null, email=null;
        private void Ok_Click(object sender, RoutedEventArgs e)
        {
            bool exist = false, correct_pass = false;
            var temp = File.ReadAllLines("Admins.csv");
            foreach (string line in temp)
            {
                var data = line.Split(',');
                if (user_text.Text == data[3])
                {
                    exist = true;
                    if (password.Text == data[4])
                    {
                        correct_pass = true;
                        name = data[0];
                        lastname = data[1];
                        email = data[2];

                    }
                }
            }
            if (exist && correct_pass)
            {
                MessageBox.Show("You successfully logged in");
                Hide();
                new Admin_Account(name, lastname, email).Show();
            }
            else if (exist && !correct_pass)
                MessageBox.Show("password is not corrext");
            else if (!exist)
                MessageBox.Show("This user doesn't exist");
        }
    }
}
