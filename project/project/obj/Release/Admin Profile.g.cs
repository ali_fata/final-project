﻿#pragma checksum "..\..\Admin Profile.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "20C1B576F432773E7F2509B76210CA7F6750CA5E5E7F53ACD8ADF32946371AC3"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using project;


namespace project {
    
    
    /// <summary>
    /// Admin_Profile
    /// </summary>
    public partial class Admin_Profile : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 14 "..\..\Admin Profile.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image ProfilePhoto;
        
        #line default
        #line hidden
        
        
        #line 19 "..\..\Admin Profile.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button ChangeProfile;
        
        #line default
        #line hidden
        
        
        #line 20 "..\..\Admin Profile.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button SavePhoto;
        
        #line default
        #line hidden
        
        
        #line 27 "..\..\Admin Profile.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox firstname;
        
        #line default
        #line hidden
        
        
        #line 28 "..\..\Admin Profile.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox Lastname;
        
        #line default
        #line hidden
        
        
        #line 31 "..\..\Admin Profile.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox email;
        
        #line default
        #line hidden
        
        
        #line 33 "..\..\Admin Profile.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox user_ID;
        
        #line default
        #line hidden
        
        
        #line 35 "..\..\Admin Profile.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox pass;
        
        #line default
        #line hidden
        
        
        #line 37 "..\..\Admin Profile.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox phone_number;
        
        #line default
        #line hidden
        
        
        #line 38 "..\..\Admin Profile.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button apply_changes;
        
        #line default
        #line hidden
        
        
        #line 39 "..\..\Admin Profile.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock applied;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/project;component/admin%20profile.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\Admin Profile.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.ProfilePhoto = ((System.Windows.Controls.Image)(target));
            return;
            case 2:
            this.ChangeProfile = ((System.Windows.Controls.Button)(target));
            
            #line 19 "..\..\Admin Profile.xaml"
            this.ChangeProfile.Click += new System.Windows.RoutedEventHandler(this.ChangeProfile_Click);
            
            #line default
            #line hidden
            return;
            case 3:
            this.SavePhoto = ((System.Windows.Controls.Button)(target));
            
            #line 20 "..\..\Admin Profile.xaml"
            this.SavePhoto.Click += new System.Windows.RoutedEventHandler(this.SavePhoto_Click);
            
            #line default
            #line hidden
            return;
            case 4:
            this.firstname = ((System.Windows.Controls.TextBox)(target));
            return;
            case 5:
            this.Lastname = ((System.Windows.Controls.TextBox)(target));
            return;
            case 6:
            this.email = ((System.Windows.Controls.TextBox)(target));
            return;
            case 7:
            this.user_ID = ((System.Windows.Controls.TextBox)(target));
            return;
            case 8:
            this.pass = ((System.Windows.Controls.TextBox)(target));
            return;
            case 9:
            this.phone_number = ((System.Windows.Controls.TextBox)(target));
            return;
            case 10:
            this.apply_changes = ((System.Windows.Controls.Button)(target));
            
            #line 38 "..\..\Admin Profile.xaml"
            this.apply_changes.Click += new System.Windows.RoutedEventHandler(this.apply_changes_Click);
            
            #line default
            #line hidden
            return;
            case 11:
            this.applied = ((System.Windows.Controls.TextBlock)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

