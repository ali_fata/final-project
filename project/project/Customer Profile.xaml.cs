﻿using System;
using Microsoft.Win32;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using System.Text.RegularExpressions;

namespace project
{
    public partial class Customer_Profile : Window
    {
        string FirstName, LastName, Email, Pass, PhoneNumber, National_ID, Address;
        Image photo;
        public Customer_Profile(string name, string lastname)
        {
            InitializeComponent();
            FirstName = name; LastName = lastname;
            account_name.Text = name + " " + lastname;
            firstname.Text = name; Lastname.Text = lastname;
            var file = File.ReadAllLines("Customers.csv");
            foreach(string line in file)
            {
                var data = line.Split(',');
                if(data[0]==name && data[1]==lastname)
                {
                    Email = data[2]; Pass = data[3]; PhoneNumber = data[4]; National_ID = data[5]; Address = data[6];
                    email.Text = Email;
                    pass.Text = Pass;
                    phone_number.Text = PhoneNumber;
                    nationak_ID.Text = National_ID;
                    address.Text = Address;
                }
            }
            string directory = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
            string filePath = Path.Combine(directory, @"C:\Users\TTM\Desktop\final project\project\project\bin\Debug\Customers\"+FirstName+" "+LastName+".jpg");
            if (File.Exists(filePath))
            {
                BitmapImage image = new BitmapImage();
                image.BeginInit();
                image.UriSource = new Uri(@"C:\Users\TTM\Desktop\final project\project\project\bin\Debug\Customers\"+FirstName+" "+LastName+".jpg");
                image.EndInit();
                ProfilePhoto.Source = image;
            }
        }
        private void apply_changes_Click(object sender, RoutedEventArgs e)
        {
            if (!check_email(email.Text))
                MessageBox.Show("Invalid Email !", "Alert", MessageBoxButton.OK, MessageBoxImage.Error);
            else if (!check_Password(pass.Text))
                MessageBox.Show("Password must contains every characters including numbers,letters,marks,...", "Alert", MessageBoxButton.OK, MessageBoxImage.Error);
            else if (!check_PhoneNumber(phone_number.Text))
                MessageBox.Show("Invalid phone number !", "Alert", MessageBoxButton.OK, MessageBoxImage.Error);
            else if (!nationak_ID.Text.Check_national_ID())
                MessageBox.Show("Invalid National ID !", "Alert", MessageBoxButton.OK, MessageBoxImage.Error);
            else
            {
                List<string> lines = new List<string>();
                if (File.Exists("Customers.csv"))
                {
                    using (StreamReader reader = new StreamReader("Customers.csv"))
                    {
                        string line;
                        while ((line = reader.ReadLine()) != null)
                        {
                            if (line.Contains(","))
                            {
                                var data = line.Split(',');
                                if (data[0] == FirstName && data[1] == LastName)
                                {
                                    data[0] = firstname.Text; FirstName = firstname.Text;
                                    data[1] = Lastname.Text; LastName = Lastname.Text;
                                    data[2] = email.Text; Email = email.Text;
                                    data[3] = pass.Text; Pass = pass.Text;
                                    data[4] = phone_number.Text; PhoneNumber = phone_number.Text;
                                    data[5] = nationak_ID.Text; National_ID = nationak_ID.Text;
                                    data[6] = address.Text; Address = address.Text;
                                    line = string.Join(",", data);
                                }
                            }
                            lines.Add(line);
                        }
                        reader.Close();
                    }
                }
                using (StreamWriter writer = new StreamWriter("Customers.csv", false))
                {
                    foreach (string line in lines)
                        writer.WriteLine(line);
                    writer.Close();
                }
                applied.Visibility = Visibility.Visible;
            }   
        }
        private void SavePhoto_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog f = new SaveFileDialog();
            string name = FirstName + " " + LastName + ".jpg";
            f.Filter = "All supported graphics|*.jpg;*.jpeg;*.png|" + "JPEG (*.jpg;*.jpeg)|*.jpg;*.jpeg|" + "Portable Network Graphic (*.png)|*.png";
            var encoder = new PngBitmapEncoder();
            encoder.Frames.Add(BitmapFrame.Create((BitmapSource)ProfilePhoto.Source));
            using (FileStream stream = new FileStream(@"C:\Users\TTM\Desktop\final project\project\project\bin\Debug\Customers\"+name+".jpg", FileMode.Create))
            { encoder.Save(stream); stream.Close(); }
        }
        private void ChangeProfile_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog op = new OpenFileDialog();
            op.Title = "Select a picture";
            op.Filter = "All supported graphics|*.jpg;*.jpeg;*.png|" + "JPEG (*.jpg;*.jpeg)|*.jpg;*.jpeg|" + "Portable Network Graphic (*.png)|*.png";
            if (op.ShowDialog() == true)
            {
                ProfilePhoto.Source = new BitmapImage(new Uri(op.FileName));
                photo = ProfilePhoto;
            }
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Hide(); new MainWindow().Show(); Close();
        }
        public static bool check_email(string input)
        {
            string strRegex = @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z";
            Regex re = new Regex(strRegex, RegexOptions.IgnoreCase);
            if (re.IsMatch(input))
                return true;
            else
                return false;
        }
        public static bool check_PhoneNumber(string input)
        {
            string r1 = @"([09]{2}[0-9]{9})", r2 = @"([9]{1}[0-9]{9})", r3 = @"([+9809]{5}[0-9]{9})", r4 = @"([00989]{5}[0-9]{9})";
            Regex Re1 = new Regex(r1), Re2 = new Regex(r2), Re3 = new Regex(r3), Re4 = new Regex(r4);
            if (Re1.IsMatch(input) || Re2.IsMatch(input) || Re3.IsMatch(input) || Re4.IsMatch(input))
                return true;
            else
                return false;
        }
        public static bool check_Password(string input)
        {
            string strRegex = @"^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#.?!@$%^&*-]).{3,}$";
            Regex re = new Regex(strRegex, RegexOptions.IgnoreCase);
            if (re.IsMatch(input))
                return true;
            else
                return false;
        }
    }
    static class Extention
    {
        public static bool Check_national_ID(this string str)
        {
            int[] ID = new int[str.Length];
            for (int i = 0; i < str.Length; i++)
                ID[i] = Convert.ToInt32(str[i]) - 48;
            int a = ID[9];
            int b = (ID[0] * 10) + (ID[1] * 9) + (ID[2] * 8) + (ID[3] * 7) + (ID[4] * 6) + (ID[5] * 5) + (ID[6] * 4) + (ID[7] * 3) + (ID[8] * 2);
            int c = b % 11;
            bool tekrari = true;
            for (int i = 0; i < ID.Length; i++)
                if (ID[0] != ID[i])           //if all the digits are the same, like 999999999
                    tekrari = false;
            if (tekrari)
                return false;
            if (c == 0 && a == c)
                return true;
            else if (c == 1 && a == 1)
                return true;
            else if (c > 1 && a == Math.Abs(c - 11))
                return true;
            else
                return false;
        }
    }
}